const chromeDriver = require("../drivers/chrome");
const { WebDriver,By} = require("selenium-webdriver");
var wd = require('selenium-webdriver'),
    SeleniumServer = require('selenium-webdriver/remote').SeleniumServer,
    request = require('request');

describe("Aura Code Challenge - Create User Account Tests", () => {
  let driver;
  beforeAll(() => {
    driver = chromeDriver();
  });

  afterAll(async () => {
    await driver.quit();
  });

  test("it loads authentication page", async () => {
    await driver.get(
      "http://automationpractice.com/index.php?controller=authentication&back=my-account"
    );
	const title = await driver.getTitle();
    expect(title).toBe("Login - My Store");
	await driver.findElement(By.xpath('//*[@id="email_create"]')).sendKeys("tester@crossbrowsertesting.com");
	await driver.findElement(By.xpath('//*[@id="SubmitCreate"]')).click();
	const radio = await driver.findElement(By.xpath('(//span/input)[1]'));
    radio.click();
	await driver.findElement(By.xpath('(//input[@type="text"])[2]')).sendKeys("subbu");
	await driver.findElement(By.xpath('(//input[@type="text"])[3]')).sendKeys("chinta");
	await driver.findElement(By.xpath('//input[@type="password"]')).sendKeys("CS0909@kns")
	
	
	function sendByTextInputs() {
    
	addfirstname = driver.findElement(By.xpath('(//input[@type="text"])[5]'));
    driver.executeScript("arguments[0].scrollIntoView()", addfirstname.sendKeys("Subrahmanyam"));
	addlastname = driver.findElement(By.xpath('(//input[@type="text"])[6]'));
    driver.executeScript("arguments[0].scrollIntoView()", addlastname.sendKeys("Chintasubrahmanyam"));
	companyname = driver.findElement(By.xpath('(//input[@type="text"])[7]'));
    driver.executeScript("arguments[0].scrollIntoView()", companyname.sendKeys("Tsssinfotech"));
	address = driver.findElement(By.xpath('(//input[@type="text"])[8]'));
    driver.executeScript("arguments[0].scrollIntoView()", address.sendKeys("Hyderabad"));
	address_line2 = driver.findElement(By.xpath('(//input[@type="text"])[9]'));
    driver.executeScript("arguments[0].scrollIntoView()", address_line2.sendKeys("Sriramnagar,Kondapur Main Road, Near Chirec public School"));
	address_City = driver.findElement(By.xpath('(//input[@type="text"])[10]'));
    driver.executeScript("arguments[0].scrollIntoView()", address_City.sendKeys("Hyderabad"));
	address_Zipcode = driver.findElement(By.xpath('(//input[@type="text"])[11]'));
    driver.executeScript("arguments[0].scrollIntoView()", address_Zipcode.sendKeys("500084"));
	Home_phone = driver.findElement(By.xpath('(//input[@type="text"])[12]'));
    driver.executeScript("arguments[0].scrollIntoView()", Home_phone.sendKeys("040 4010 0468"));
	mobile_phone = driver.findElement(By.xpath('(//input[@type="text"])[13]'));
    driver.executeScript("arguments[0].scrollIntoView()", mobile_phone.sendKeys("9154825986"));
	persanal_address = driver.findElement(By.xpath('(//input[@type="text"])[14]'));
    driver.executeScript("arguments[0].scrollIntoView()", Home_phone.sendKeys("Nagayalanka,krishana"));
    
	const text =  driver.findElement(By.xpath('//*[@id="columns"]/div[1]/span[2]')).getText()
			console.log(text)
			if(text === 'My account') {
         console.log('Test passed');
      } else {
         console.log('Test failed');
      }
		
};
	

    
  });
});
